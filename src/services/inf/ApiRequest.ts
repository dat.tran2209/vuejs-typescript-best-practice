

export default class ApiRequest {    
    public static async get(url: string) : Promise<any>{
        const axios = (await import('axios')).default;
        const data = await axios.get(url);

        return data;
    }
}