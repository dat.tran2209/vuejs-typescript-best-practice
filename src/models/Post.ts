export default class Post {
    userId: number | undefined
    id: number | undefined
    title: string | undefined
    body: string | undefined

    public getFull(): string {
        return this.title && this.body ? this.title + this.body : ''
    }
}